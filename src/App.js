import './App.css';
import { useState } from "react"; 
import { Badge, Button} from "evergreen-ui"; 

function App() {
  const [ work, setwork ] = useState('')
  const [ list, addList ] = useState([]);
  const [ counter, setCounter ] = useState(list.length);
  const [ col, setCol] = useState([])
  const [ check, setCheck] = useState('false')
  const [ num, setNum ] = useState(0)

  const adList = (work) => {
     addList(prev => [...prev, work])
     setCounter(counter+1)
     setwork('')
   }

  const setRed = (ele) => {
    setCol(prev => [...prev, ele])
  }

  const isIn = (ele) => {
    console.log(ele)
    return(col.includes(ele))
  } 

  const setChange = (index) => {
    setwork(list[index])
    setCheck('true')
    setNum(index)
    console.log(index)
  }

  const removeWork = (index) =>{
    list.splice(list.indexOf(list[index]),1)
  }
  
  return (
    <div className="App">
      <input type="text"
              value={work} 
              style={{margin: 50}}
              onChange={event => 
                  {setwork(event.target.value)}
              }
              onKeyPress={event => {
                if (event.key === 'Enter' && work !== "" && check === "false") {adList(work); console.log(col)}
                if (event.key === 'Enter' && work !== "" && check === "true") {list[num] = work; setCheck('false'); setwork('')}
              }}/>
      <h1>{counter || "nothing"} needs to be done</h1>
      {list.map((list, index) => (
        <div key={index} style={{margin: 25, textAlign:true, backgroundColor: "#B4D3B2", outerWidth: 100000}}>
          <Badge color={isIn(list) ? 'red' : 'green'} 
                  isInteractive
                  marginRight={8} margin={15} 
                  textAlign={'center'} 
                  paddingRight={40} 
                  paddingLeft={40}>
                    {list}
          </Badge>
          <div>
          <Button onClick={() => {setChange(index)}}> Change </Button>
          <Button onClick={() => {setRed(list)}}> Done </Button>
          <Button onClick={() => {removeWork(index);setCounter(counter-1)}}> Remove Work </Button>
          </div>
        </div>        
      ))}
    </div>
  );
}

export default App;
